use crate::error::BoxError;

pub trait ValidateCommandOrQuery {
    fn validate() -> Option<BoxError>;
}
