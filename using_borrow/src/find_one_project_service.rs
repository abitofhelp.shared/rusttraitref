use crate::error::BoxError;
use crate::find_one_project_query::FindOneProjectQuery;
use crate::find_one_project_query_port::FindOneProjectQueryPort;
use crate::find_one_project_usecase::FindOneProjectUseCase;
use crate::project::Project;

pub struct FindOneProjectService<'a> {
    query_port: Box<(dyn FindOneProjectQueryPort<'a>)>
}

impl <'a>FindOneProjectService<'a> {
    pub fn new(query_port: Box<(dyn FindOneProjectQueryPort<'a>)>) -> Self {
        Self { query_port }
    }
}

impl <'a>FindOneProjectUseCase<'a> for FindOneProjectService<'a> {
    fn find_one(&self, query: FindOneProjectQuery) -> Result<Project, BoxError> {
        self.query_port.find_one(query.name)
    }
}