use crate::app_id::AppId;
use crate::error::BoxError;
use crate::find_one_project_query_port::FindOneProjectQueryPort;
use crate::find_one_project_repository::FindOneProjectRepository;
use crate::project::Project;
use crate::project_entity_mapper::ProjectEntityMapper;

pub struct FindOneProjectPersistenceAdapter<'a> {
    repository: FindOneProjectRepository<'a>,
}

impl <'a>FindOneProjectPersistenceAdapter<'a> {
    pub fn new(repository: FindOneProjectRepository<'a>) -> Self {
        FindOneProjectPersistenceAdapter { repository }
    }
}

impl <'a>FindOneProjectQueryPort<'a> for FindOneProjectPersistenceAdapter<'a> {
    fn find_one(&self, name: AppId) -> Result<Project, BoxError> {
        let pe = self.repository.find_one(name.to_string()).unwrap();
        Ok(ProjectEntityMapper::to_model(pe))
    }
}