use crate::app_id::AppId;

// Represents a list of projects
pub type ProjectList = Vec<Project>;

// Represents a project
#[cfg_attr(feature = "enable_serde", derive(serde::Deserialize, serde::Serialize))]
#[derive(Clone, Debug, Default, Eq, PartialEq)]
#[non_exhaustive]
pub struct Project {
    pub name: AppId,
    pub size: usize,
    pub timezone: String,
}

impl Project {
    #[inline]
    pub fn new(name: String, size: usize, timezone: String) -> Self {
        Self {
            name: AppId::new(name),
            size,
            timezone,
        }
    }

    #[inline]
    pub fn from(name: &str, size: usize, timezone: &str) -> Self {
        Self::new(name.to_owned(), size, timezone.to_owned())
    }
}
