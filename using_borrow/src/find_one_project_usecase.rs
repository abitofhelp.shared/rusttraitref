use crate::error::BoxError;
use crate::find_one_project_query::FindOneProjectQuery;
use crate::project::Project;

// Represents the use case for finding a single project.
// Validation of input happens in the Commands/Queries.
// Validation of business rules happens in the usecase.
// Commands/Queries are immutable. so after validation we are certain that it contains valida input data.
// Validating a business rule requires access to the current state of the domain model.
pub trait FindOneProjectUseCase<'a> {
    fn find_one(&self, query: FindOneProjectQuery) -> Result<Project, BoxError>;
}
