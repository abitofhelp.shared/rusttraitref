use std::sync::Arc;
use parking_lot::RwLock;
use crate::database_connection::DatabaseConnection;
use crate::error::BoxError;
use crate::project_entity::ProjectEntity;

pub type InMemoryDatabase = Arc<RwLock<Vec<ProjectEntity>>>;

pub type InMemoryDatabaseConnection = DatabaseConnection<InMemoryDatabase>;

impl InMemoryDatabaseConnection {
    pub fn new() -> Result<Self, BoxError> {
        let db = Arc::new(RwLock::new(vec![
            ProjectEntity::new(None, "abc".to_string(), 123, "America_Phoenix".to_string()).unwrap(),
            ProjectEntity::new(None, "xyz".to_string(), 321, "America_Chicago".to_string()).unwrap(),
        ]));

        Ok(DatabaseConnection { database: db })
    }
}