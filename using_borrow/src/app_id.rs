use std::cmp::PartialEq;
use std::fmt;

// Represents the applications unique identifier -- NOT the persistence system's identifier.
#[cfg_attr(feature = "enable_serde", derive(serde::Deserialize, serde::Serialize))]
#[derive(Clone, Debug, Default, Eq, PartialEq)]
#[non_exhaustive]
pub struct AppId {
    value: String,
}

impl AppId {
    #[inline]
    pub fn new(value: String) -> Self {
        AppId { value }
    }
}

impl From<&str> for AppId {
    #[inline]
    fn from(value: &str) -> Self {
        Self::new(value.to_owned())
    }
}

impl fmt::Display for AppId {
    #[inline]
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "{}", &self.value)
    }
}
