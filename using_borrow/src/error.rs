use std::error::Error;
use thiserror::Error;

pub type BoxError = Box<dyn Error + Send + Sync>;

#[derive(Error, Debug)]
#[non_exhaustive]
pub enum ApiError {
    #[error("application error(code=500): {message}")]
    Application {
        message: String,
        err: Option<Box<dyn Error>>,
    },

    #[error("request is malformed(code=400): {message}")]
    BadRequest {
        message: String,
        err: Option<Box<dyn Error>>,
    },

    #[error("request conflicts with an existing resource(code=409): {message}")]
    Conflict {
        message: String,
        err: Option<Box<dyn Error>>,
    },

    #[error("resource not allowed(code=403): {message}")]
    Forbidden {
        message: String,
        err: Option<Box<dyn Error>>,
    },

    #[error("requested resource was not found(code=404): {message}")]
    NotFound {
        message: String,
        err: Option<Box<dyn Error>>,
    },

    #[error("not authenticated or token expired(code=401): {message}")]
    Unauthorized {
        message: String,
        err: Option<Box<dyn Error>>,
    },

    #[error("unknown error(code=500): {message}")]
    Unknown {
        message: String,
        err: Option<Box<dyn Error>>,
    },
}
