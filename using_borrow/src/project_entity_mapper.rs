use crate::project::{Project, ProjectList};
use crate::project_entity::ProjectEntity;

pub struct ProjectEntityMapper {}

impl ProjectEntityMapper {
    pub fn to_model(
        entity: ProjectEntity,
    ) -> Project {
        Project::new(
            entity.name,
            entity.size,
            entity.timezone,
        )
    }

    pub fn to_models(
        entities: Vec<ProjectEntity>,
    ) -> ProjectList {
        entities
            .iter()
            .map(|p| ProjectEntityMapper::to_model(p.clone()))
            .collect()
    }

    pub fn to_entity(
        model: Project,
    ) -> ProjectEntity {
        ProjectEntity::new(
            None,
            model.name.to_string(),
            model.size,
            model.timezone,
        )
        .unwrap()
    }
}
