use std::io::{Error, ErrorKind};
use crate::error::BoxError;
use crate::inmemory_database_connection::InMemoryDatabaseConnection;
use crate::project_entity::ProjectEntity;

pub struct FindOneProjectRepository<'a> {
    conn: &'a InMemoryDatabaseConnection,
}

impl <'a>FindOneProjectRepository<'a> {
    pub fn new_with_db(conn: &'a InMemoryDatabaseConnection) -> Result<FindOneProjectRepository<'a>, BoxError> {
        Ok( FindOneProjectRepository { conn } )
    }

    pub fn find_one(&self, name: String) -> Result<ProjectEntity, BoxError> {
        match self.conn.database.read().iter().find(|p| p.name.to_string() == name) {
            None => {
                Err(BoxError::try_from(Error::new(
                    ErrorKind::NotFound,
                    format!("failed to find a project named '{}'", name),
                ))
                    .unwrap())
                //Err(BoxError::try_from(ApiError::NotFound { message: format!("failed to find a project named '{}'", name), err: None }).unwrap())
            }
            Some(p) => Ok(p.clone()),
        }
    }
}
