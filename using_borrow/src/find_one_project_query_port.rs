use crate::app_id::AppId;
use crate::error::BoxError;
use crate::project::Project;

pub trait FindOneProjectQueryPort<'a>: 'a {
    fn find_one(&self, name: AppId) -> Result<Project, BoxError>;
}
