use crate::database_connection::DatabaseConnection;
use crate::error::{ApiError, BoxError};
use crate::project_entity::ProjectEntity;
use parking_lot::RwLock;
use std::sync::Arc;

pub type InMemoryDatabase = RwLock<Vec<ProjectEntity>>;

pub type InMemoryDatabaseConnection = Arc<DatabaseConnection<InMemoryDatabase>>;

pub fn new() -> Result<InMemoryDatabaseConnection, ApiError> {
    let inmemory_database = RwLock::new(vec![
        ProjectEntity::new(None, "abc".to_string(), 123, "America_Phoenix".to_string()).unwrap(),
        ProjectEntity::new(None, "xyz".to_string(), 321, "America_Chicago".to_string()).unwrap(),
    ]);

    if true {
        Ok(InMemoryDatabaseConnection::from(DatabaseConnection {
            database: inmemory_database,
        }))
    } else {
        Err(ApiError::Unknown {
            message: "unexpected error".to_string(),
            err: None,
        })
        .unwrap()
    }
}
