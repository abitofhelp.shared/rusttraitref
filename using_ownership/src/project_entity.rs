use crate::error::{ApiError, BoxError};
use uuid::Uuid;

// Represents a list of project entities.
pub type ProjectEntityList = Vec<ProjectEntity>;

// Represents a project entity in the database.
#[derive(Clone, Debug, Default, Eq, PartialEq)]
#[non_exhaustive]
pub struct ProjectEntity {
    pub id: String,
    pub name: String,
    pub size: usize,
    pub timezone: String,
}

impl ProjectEntity {
    #[inline]
    pub fn new(
        id: Option<String>,
        name: String,
        size: usize,
        timezone: String,
    ) -> Result<Self, ApiError> {
        Self::try_from(id.as_deref(), name, size, timezone)
    }

    #[inline]
    pub fn try_from(
        id: Option<&str>,
        name: String,
        size: usize,
        timezone: String,
    ) -> Result<Self, ApiError> {
        let eid = if let Some(id) = id {
            match Uuid::parse_str(id) {
                Ok(i) => i,
                Err(e) => {
                    return Err(ApiError::Application {
                        message: "failed to convert the 'id' &str to a Uuid".to_string(),
                        err: Some(BoxError::try_from(e).unwrap()),
                    })
                }
            }
        } else {
            // No id provided, so we will make one.
            Uuid::new_v4()
        };

        Ok(Self {
            id: eid.to_string(),
            name,
            size,
            timezone,
        })
    }
}
