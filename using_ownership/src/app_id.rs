use std::cmp::PartialEq;
use std::fmt;

// Represents the applications unique identifier -- NOT the persistence system's identifier.
#[derive(Clone, Debug, Default, Eq, PartialEq)]
#[non_exhaustive]
pub struct AppId {
    value: String,
}

impl AppId {
    #[inline]
    pub const fn new(value: String) -> Self {
        Self { value }
    }
}

impl From<&str> for AppId {
    #[inline]
    fn from(value: &str) -> Self {
        Self::new(value.to_owned())
    }
}

impl fmt::Display for AppId {
    #[inline]
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "{}", &self.value)
    }
}
