use crate::error::{ApiError, BoxError};

pub trait ValidateCommandOrQuery {
    fn validate() -> Option<ApiError>;
}
