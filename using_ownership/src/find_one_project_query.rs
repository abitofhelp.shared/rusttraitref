use crate::app_id::AppId;
use crate::error::{ApiError, BoxError};
use crate::validate_command_or_query::ValidateCommandOrQuery;

// Represents the input model for the usecase.
// Validation of input happens in the Commands/Queries.
// Commands/Queries are immutable. so after validation we are certain that it contains valida input data.
// Validating a command/query DOES NOT require access to the current state of the domain model.
pub struct FindOneProjectQuery {
    pub(crate) name: AppId,
}

impl FindOneProjectQuery {
    pub fn new(name: AppId) -> Result<Self, ApiError> {
        <Self as ValidateCommandOrQuery>::validate().map_or_else(|| Ok(Self { name }), |e| Err(e))
    }
}

impl From<&str> for FindOneProjectQuery {
    fn from(value: &str) -> Self {
        Self {
            name: AppId::from(value),
        }
    }
}

impl ValidateCommandOrQuery for FindOneProjectQuery {
    fn validate() -> Option<ApiError> {
        None
    }
}
