mod app_id;
mod database_connection;
mod error;
mod find_one_project_persistence_adapter;
mod find_one_project_query;
mod find_one_project_query_port;
mod find_one_project_repository;
mod find_one_project_service;
mod find_one_project_usecase;
mod inmemory_database_connection;
mod project;
mod project_entity;
mod project_entity_mapper;
mod validate_command_or_query;

use crate::find_one_project_persistence_adapter::FindOneProjectPersistenceAdapter;
use crate::find_one_project_query::FindOneProjectQuery;
use crate::find_one_project_repository::FindOneProjectRepository;
use crate::find_one_project_service::FindOneProjectService;
use crate::find_one_project_usecase::FindOneProjectUseCase;
use crate::inmemory_database_connection::InMemoryDatabaseConnection;

fn main() -> std::io::Result<()> {
    let database_connection = inmemory_database_connection::new().unwrap();
    find_one_project(database_connection);

    Ok(())
}

fn find_one_project(database_connection: InMemoryDatabaseConnection) {
    let find_one_project_repository =
        FindOneProjectRepository::new_from_connection(database_connection).unwrap();
    let find_one_project_persistence_adapter =
        FindOneProjectPersistenceAdapter::new(find_one_project_repository);

    let find_one_project_service =
        FindOneProjectService::new(&find_one_project_persistence_adapter);

    let find_one_project_query = FindOneProjectQuery::from("abc");
    match find_one_project_service.find_one(find_one_project_query) {
        Ok(l) => {
            println!("{l:?}");
        }
        Err(e) => {
            println!("{e:?}");
        }
    }
}
