use crate::error::{ApiError, BoxError};
use crate::find_one_project_query::FindOneProjectQuery;
use crate::find_one_project_query_port::FindOneProjectQueryPort;
use crate::find_one_project_usecase::FindOneProjectUseCase;
use crate::project::Project;

pub struct FindOneProjectService<'a> {
    query_port: &'a dyn FindOneProjectQueryPort,
}

impl<'a> FindOneProjectService<'a> {
    pub const fn new(query_port: &'a dyn FindOneProjectQueryPort) -> Self {
        Self { query_port }
    }
}

impl FindOneProjectUseCase for FindOneProjectService<'_> {
    fn find_one(&self, query: FindOneProjectQuery) -> Result<Project, ApiError> {
        self.query_port.find_one(query.name)
    }
}
