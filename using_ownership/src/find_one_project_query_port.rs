use crate::app_id::AppId;
use crate::error::{ApiError, BoxError};
use crate::project::Project;

pub trait FindOneProjectQueryPort {
    fn find_one(&self, name: AppId) -> Result<Project, ApiError>;
}
