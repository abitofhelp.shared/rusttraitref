use crate::app_id::AppId;
use crate::error::{ApiError, BoxError};
use crate::find_one_project_query_port::FindOneProjectQueryPort;
use crate::find_one_project_repository::FindOneProjectRepository;
use crate::project::Project;
use crate::project_entity_mapper::ProjectEntityMapper;

pub struct FindOneProjectPersistenceAdapter {
    repository: FindOneProjectRepository,
}

impl FindOneProjectPersistenceAdapter {
    pub const fn new(repository: FindOneProjectRepository) -> Self {
        Self { repository }
    }
}

impl FindOneProjectQueryPort for FindOneProjectPersistenceAdapter {
    fn find_one(&self, name: AppId) -> Result<Project, ApiError> {
        let pe = self.repository.find_one(name.to_string()).unwrap();
        Ok(ProjectEntityMapper::to_model(pe))
    }
}
