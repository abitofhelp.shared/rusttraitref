use crate::error::{ApiError, BoxError};
use crate::inmemory_database_connection::InMemoryDatabaseConnection;
use crate::project_entity::ProjectEntity;
use std::io::{Error, ErrorKind};

pub struct FindOneProjectRepository {
    conn: InMemoryDatabaseConnection,
}

impl FindOneProjectRepository {
    pub fn new_from_connection(conn: InMemoryDatabaseConnection) -> Result<Self, ApiError> {
        // With a more complex database, it is likely that there would be errors, which would
        // need to return a Result<>.  The inmemory database only has a success condition, so
        // I satisfy the Err condition despite that it will never happen.
        if true {
            Ok(Self { conn })
        } else {
            Err(ApiError::Unknown {
                message: "unexpected error creating an instance with the database connection"
                    .to_string(),
                err: None,
            })
        }
    }

    pub fn find_one(&self, name: String) -> Result<ProjectEntity, ApiError> {
        self.conn
            .database
            .read()
            .iter()
            .find(|p| p.name == name)
            .map_or_else(
                || {
                    Err(ApiError::NotFound {
                        message: format!("failed to find a project named '{name}'"),
                        err: None,
                    })
                },
                |pe| Ok(pe.clone()),
            )
    }
}
